import styled from "styled-components";

export const Container = styled.div`
	display: flex;
	margin: auto;
	position: relative;
	width: 100%;
	height: 100vh;
`;

export const Slide = styled.div`
	display: flex;
	overflow: hidden;
	width: 100%;
	height: 100vh;
`;

export const WelcomeContent = styled.div`
	align-items: center;
	display: flex;
	flex-direction: column;
	gap: 1rem;
	left: 50%;
	position: absolute;
	text-align: center;
	top: 50%;
	transform: translate(-50%, -50%);
`;

export const WelcomeTitle2 = styled.div`
	background: linear-gradient(
			91.73deg,
			#15ffdb -21.03%,
			#1593ff 52.98%,
			#0f3b5d 116.24%
		),
		linear-gradient(0deg, #ffffff, #ffffff);
	font-family: ${(props) => props.theme.fonts.primaryFont};
	font-size: 6.4rem;
	font-weight: 800;
	letter-spacing: -0.015rem;
	line-height: 6.7rem;
	-webkit-background-clip: text;
	-webkit-text-fill-color: transparent;

	@media (max-width: 768px) {
		font-size: 3.2rem;
		line-height: 3.3rem;
	}
`;

export const ImgBackground = styled.img`
	animation-duration: 12s;
	animation-iteration-count: infinite;
	animation-name: autoplay;
	display: flex;
	flex-direction: column;
	flex-grow: 0;
	flex-shrink: 0;
	/* height: 80rem; */
	height: 100vh;

	width: 100%;

	@keyframes autoplay {
		0% {
			transform: translateX(0%);
		}
		33% {
			transform: translateX(-100%);
		}
		66% {
			transform: translateX(-200%);
		}
	}
`;

export const Gradient = styled.div`
	background: linear-gradient(180deg, #0c171d 0%, rgba(12, 23, 29, 0.6) 100%);
	height: 80rem;
	opacity: 0.7;
	position: absolute;
	width: 100%;
	height: 100vh;

	z-index: 0;
`;

export const WelcomeContainer = styled.div`
	display: flex;
	flex-direction: column;
	gap: 1.5rem;
	text-align: center;
`;

export const WelcomeText = styled.div`
	color: ${(props) => props.theme.colors.white};
	font-family: ${(props) => props.theme.fonts.primaryFont};
	font-size: 2rem;
	font-weight: 500;
	letter-spacing: 0;
	line-height: 1.9rem;
	opacity: 0.7;
`;

export const WelcomeTitleContainer = styled.div`
	display: flex;
	flex-direction: column;
	gap: 2rem;

	@media (max-width: 768px) {
		gap: 1rem;
	}
`;

export const WelcomeTitle = styled.div`
	color: ${(props) => props.theme.colors.white};
	font-family: ${(props) => props.theme.fonts.primaryFont};
	font-size: 6.4rem;
	font-weight: 800;
	letter-spacing: -0.015rem;
	line-height: 6.7rem;

	@media (max-width: 768px) {
		font-size: 3.2rem;
		line-height: 3.3rem;
	}
`;

export const Button = styled.a`
	align-items: center;
	background: ${(props) => props.theme.colors.blueColor};
	border-radius: 1.6rem;
	border: none;
	color: ${(props) => props.theme.colors.white};
	display: flex;
	gap: 0.7rem;
	justify-content: center;
	margin-top: 3rem;
	padding: 2.3rem 2.8rem;

	div {
		font-family: ${(props) => props.theme.fonts.primaryFont};
		font-size: 1.6rem;
		font-weight: 700;
		letter-spacing: 0;
		line-height: 1.9rem;
	}

	img {
		margin-top: 0.2rem;
		width: 1rem;
	}

	&:hover {
		opacity: 0.9;
		cursor: pointer;
	}
`;
