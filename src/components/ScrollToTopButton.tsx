import { useEffect, useState } from "react";
import { ScrollButton } from "./styled";

function ScrollToTopButton() {
	const [isVisible, setIsVisible] = useState(false);
	useEffect(() => {
		const toggleVisibility = () => {
			if (window.pageYOffset > 300) {
				setIsVisible(true);
			} else {
				setIsVisible(false);
			}
		};

		window.addEventListener("scroll", toggleVisibility);

		return () => window.removeEventListener("scroll", toggleVisibility);
	}, []);

	const scrollToTop = () => {
		window.scrollTo({
			top: 0,
			behavior: "smooth",
		});
	};
	return (
		<ScrollButton visible={isVisible} onClick={scrollToTop}>
			&#8679;
		</ScrollButton>
	);
}

export default ScrollToTopButton;
