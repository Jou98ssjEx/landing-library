import { Swiper, SwiperSlide } from "swiper/react";
import { MainSection, MainTitle, SubTitle } from "./styled";
import {
	Container,
	Gradient,
	ImgBackground,
	Slide,
	WelcomeContent,
	WelcomeText,
	WelcomeTitle,
	WelcomeTitle2,
	WelcomeTitleContainer,
} from "./Home.styled";

function Home() {
	return (
		<Container id='home'>
			<Slide>
				<ImgBackground
					src='https://img.freepik.com/fotos-premium/laptop-fondo…a-digital-muchos-libros-pantalla_662214-30150.jpg'
					alt='bg'
				/>
				<ImgBackground
					src='https://img.freepik.com/fotos-premium/biblioteca-l…a-llena-estanteria-ia-generativa_438099-15531.jpg'
					alt='bg'
				/>
				<ImgBackground
					src='https://www.shutterstock.com/image-photo/elearning…tion-concept-learning-online-600nw-1865958031.jpg'
					alt='bg'
				/>
			</Slide>
			<Gradient />
			<WelcomeContent>
				<WelcomeTitleContainer>
					<WelcomeTitle>Biblioteca digital </WelcomeTitle>
					<WelcomeTitle2> ProQuest</WelcomeTitle2>
				</WelcomeTitleContainer>
				<WelcomeText>
					Bibliteca digital que ofrece acceso a una gran cantidad de contenido
					académico, incluyendo revistas, periódicos, tesis, informes y otros
					recursos en varias disciplinas.
				</WelcomeText>
			</WelcomeContent>
		</Container>
	);
}

export default Home;
