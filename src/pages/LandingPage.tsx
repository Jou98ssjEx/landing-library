import { useLocation, useNavigate } from "react-router-dom";
import DigitalLibrary from "../components/DigitalLibrary";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Home from "../components/Home";
import MisionAndVision from "../components/MisionAndVision";
import Policies from "../components/Policies";
import ScrollToTopButton from "../components/ScrollToTopButton";
import UserTypes from "../components/UserType";
import { useEffect } from "react";

function LandingPage() {
	const navigate = useNavigate();
	const { pathname } = useLocation();
	useEffect(() => {
		const handleScroll = () => {
			const sections = document.querySelectorAll("section[id]");
			const scrollPosition = window.scrollY + 100;

			sections.forEach((section: any) => {
				const sectionTop = section.offsetTop;
				const sectionHeight = section.offsetHeight;
				const sectionId = section.getAttribute("id");

				if (
					scrollPosition >= sectionTop &&
					scrollPosition <= sectionTop + sectionHeight
				) {
					if (`#${sectionId}` !== pathname) {
						navigate(`#${sectionId}`);
					}
				}
			});
		};

		window.addEventListener("scroll", handleScroll);
		return () => window.removeEventListener("scroll", handleScroll);
	}, [navigate, pathname]);

	return (
		<>
			<Header />
			<Home />
			<MisionAndVision />
			<UserTypes />
			<Policies />
			<DigitalLibrary />
			<Footer />
			<ScrollToTopButton />
		</>
	);
}

export default LandingPage;
