import { Fragment, useState } from "react";
import {
	CircleItemContainer,
	GeneralSeccion,
	ListItem,
	ListItemConent,
	PolicFirstItemContainer,
	PoliciesContent,
	PolicyColumn,
	PolicyDescription,
	PolicyItem,
	PolicyNumber,
	PolicyRow,
	PolicyTitle,
	SubItem,
	SubItemsContainer,
} from "./styled";
import ListPoliceItem from "./ListPoliceItem";

function Policies() {
	const [isSubItemsOpen, setIsSubItemsOpen] = useState(false);

	const toggleSubItems = () => {
		setIsSubItemsOpen(!isSubItemsOpen);
	};

	const data = [
		{
			title: "Selección de contenido",
			description:
				"Las bibliotecas digitales tienen políticas establecidas para la selección de contenido, que pueden incluir criterios como relevancia académica, calidad, actualidad y demanda de los usuarios.",
		},
		{
			title: "Acceso abierto vs. acceso restringido",
			description:
				"Algunas bibliotecas digitales ofrecen acceso abierto a su contenido, mientras que otras pueden restringir el acceso a determinados recursos a usuarios autorizados, como suscriptores institucionales.",
		},
		{
			title: "Política de adquisición y adición de nuevos materiales",
			description:
				"Las bibliotecas digitales suelen tener políticas para la adquisición y la adición de nuevos materiales a sus colecciones, lo que puede incluir la compra de licencias de contenido, la adquisición de materiales de dominio público y la colaboración con editores y proveedores de contenido.",
		},
		{
			title: "Política de gestión de derechos de autor y licencias",
			description:
				"Las bibliotecas digitales deben cumplir con las leyes de derechos de autor y las licencias de uso de los materiales que ofrecen. Esto puede implicar el seguimiento y la gestión de los derechos de autor, la obtención de permisos para el uso de ciertos materiales y la implementación de medidas de protección de derechos de autor.",
		},
		{
			title: "Política de preservación digital",
			description:
				"La preservación digital es fundamental para garantizar la accesibilidad a largo plazo del contenido digital. Las bibliotecas digitales suelen tener políticas y procedimientos para la preservación y el mantenimiento de sus colecciones digitales, que pueden incluir la creación de copias de seguridad, la migración a formatos de archivo sostenibles y la implementación de medidas de seguridad contra la pérdida de datos.",
		},
		{
			title: "Política de desacceso",
			description:
				"En algunos casos, las bibliotecas digitales pueden implementar políticas de desacceso para retirar o restringir el acceso a ciertos materiales de sus colecciones. Estas decisiones pueden basarse en consideraciones como la obsolescencia del contenido, la falta de demanda o cambios en las políticas de adquisición y acceso.",
		},
		{
			title: "Política de gestión de metadatos",
			description:
				"Los metadatos desempeñan un papel importante en la organización y recuperación de la información en las bibliotecas digitales. Las bibliotecas suelen tener políticas para la creación, gestión y mantenimiento de metadatos, que incluyen estándares de metadatos, vocabularios controlados y prácticas de catalogación consistentes.",
		},
	];

	return (
		<GeneralSeccion bg='#dce5f4' id='policies'>
			<PoliciesContent>
				<PolicyTitle>Políticas de Desarrollo de Colecciones</PolicyTitle>

				<PolicyRow>
					{data.map((item, idx) => {
						return (
							<ListPoliceItem key={idx} idx={idx} item={item} />
							// <Fragment key={idx}>
							// 	<PolicFirstItemContainer
							// 		style={{ justifyContent: "center" }}
							// 		key={idx}>
							// 		<CircleItemContainer> {idx + 1} </CircleItemContainer>
							// 		<ListItem>
							// 			<ListItemConent>
							// 				<PolicyDescription>{item.title}</PolicyDescription>
							// 				<i className='pi pi-chevron-right'></i>
							// 			</ListItemConent>
							// 		</ListItem>
							// 	</PolicFirstItemContainer>

							// 	<PolicFirstItemContainer
							// 		style={{ marginRight: "5rem" }}
							// 		key={idx}
							// 		isActive>
							// 		<PolicyDescription>{item.description}</PolicyDescription>
							// 	</PolicFirstItemContainer>
							// </Fragment>
						);
					})}
					{/* <PolicyColumn>
						<PolicyItem onClick={toggleSubItems}>
							<PolicyNumber>1</PolicyNumber>
							<PolicyDescription>
								Descripción de la política 1.
							</PolicyDescription>
							<SubItemsContainer isOpen={isSubItemsOpen}>
								<SubItem>Subelemento 1</SubItem>
								<SubItem>Subelemento 2</SubItem>
								<SubItem>Subelemento 3</SubItem>
							</SubItemsContainer>
						</PolicyItem>
					</PolicyColumn>
					<PolicyColumn>
						<PolicyItem onClick={toggleSubItems}>
							<PolicyNumber>1</PolicyNumber>
							<PolicyDescription>
								Descripción de la política 1.
							</PolicyDescription>
							<SubItemsContainer isOpen={isSubItemsOpen}>
								<SubItem>Subelemento 1</SubItem>
								<SubItem>Subelemento 2</SubItem>
								<SubItem>Subelemento 3</SubItem>
							</SubItemsContainer>
						</PolicyItem>
					</PolicyColumn>
					<PolicyColumn>
						<PolicyItem onClick={toggleSubItems}>
							<PolicyNumber>1</PolicyNumber>
							<PolicyDescription>
								Descripción de la política 1.
							</PolicyDescription>
							<SubItemsContainer isOpen={isSubItemsOpen}>
								<SubItem>Subelemento 1</SubItem>
								<SubItem>Subelemento 2</SubItem>
								<SubItem>Subelemento 3</SubItem>
							</SubItemsContainer>
						</PolicyItem>
					</PolicyColumn> */}
					{/* Aquí van más columnas de políticas */}
				</PolicyRow>
			</PoliciesContent>
		</GeneralSeccion>
	);
}

export default Policies;
