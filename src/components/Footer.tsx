import styled from "styled-components";
import {
	Copyright,
	FooterColumn,
	FooterContent,
	FooterRow,
	FooterText,
	FooterText2,
	FooterTitle,
	GeneralSeccion,
	StudentImage,
} from "./styled";

import Image1 from "../assets/user1.jpeg";
import Image2 from "../assets/user2.jpeg";
import Image3 from "../assets/user3.jpeg";
import { Fragment } from "react";

const data = [
	{
		name: "Rosa Margarita Parrales Delgado",
		image: Image1,
		email: "rparrales2463@utm.edu.ec",
	},
	{
		name: "María Dolores Vélez",
		image: Image2,
		email: "mvelez4132@utm.edu.ec",
	},
	{
		name: "Katiuska Molina Velasquez",
		image: Image3,
		email: "kmolina0319@utm.edu.ec",
	},
];

function Footer() {
	return (
		<GeneralSeccion id='footer' bg='#706f6f'>
			{/* <FooterContainer>Footer content goes here</FooterContainer> */}
			<FooterContent>
				<FooterTitle>Equipo del proyecto</FooterTitle>
				<FooterRow>
					{data.map((item, idx) => (
						<div
							style={{ display: "flex", flexDirection: "column", gap: "2rem" }}
							key={idx}>
							<FooterColumn>
								<StudentImage src={item.image} />
							</FooterColumn>
							<FooterColumn>
								<FooterText>{item.name}</FooterText>
								<FooterText2>{item.email}</FooterText2>
							</FooterColumn>
						</div>
					))}
				</FooterRow>

				<Copyright>&copy; Copyright Derechos Reservados 2024</Copyright>
			</FooterContent>
		</GeneralSeccion>
	);
}

export default Footer;
