import styled from "styled-components";
import { HeaderContainer, HeaderContent, NavItem, NavItems } from "./styled";
import { NavLink, useLocation } from "react-router-dom";
import { Link } from "react-router-dom";

const navItems = [
	{
		id: "home",
		name: "Home",
	},
	{
		id: "users",
		name: "Usuarios",
	},
	{
		id: "mission-vision",
		name: "Misión y Visión",
	},
	{
		id: "policies",
		name: "Políticas",
	},
	{
		id: "libraries",
		name: "Bibliotecas",
	},
];
function Header() {
	const { hash } = useLocation();
	const scrollIntoViewTop = (id: string) => {
		document.getElementById(id)!.scrollIntoView({ behavior: "smooth" });
	};

	return (
		<HeaderContainer>
			<HeaderContent>
				<NavItems>
					<ul>
						{navItems.map((item: any) => (
							<NavItem key={item.id} isBackgroundBlack>
								<Link
									className={hash === `#${item.id}` ? "active" : ""}
									onClick={() => scrollIntoViewTop(item.id)}
									to={`/#${item.id}`}>
									{item.name}
								</Link>
							</NavItem>
						))}
					</ul>
				</NavItems>
				{/* <Nav>
					<NavLink to='#home'>Inicio</NavLink>
					<NavLink to='#users'>Usuarios</NavLink>
					<NavLink to='#mission-vision'>Misión y Visión</NavLink>
					<NavLink to='#policies'>Políticas</NavLink>
					<NavLink to='#libraries'>Bibliotecas</NavLink>
				</Nav> */}
			</HeaderContent>
		</HeaderContainer>
	);
}

export default Header;
