import {
	GeneralSeccion,
	MissionVisionColumn,
	MissionVisionContent,
	MissionVisionDescription,
	MissionVisionRow,
	MissionVisionTitle,
} from "./styled";

function MisionAndVision() {
	return (
		<GeneralSeccion bg='#ede8e4' id='mission-vision'>
			{/* <GeneralSeccion bg='#8da7baaa' id='mission-vision'> */}
			<MissionVisionContent>
				<MissionVisionTitle>Misión y Visión</MissionVisionTitle>
				<MissionVisionRow>
					<img
						src='https://contabilidad360.files.wordpress.com/2016/10/mision.png'
						alt='img'
					/>
					<MissionVisionColumn>
						<MissionVisionDescription>
							La misión de ProQuest es facilitar el descubrimiento y el acceso a
							información valiosa y confiable para usuarios de todo el mundo,
							ayudándoles a alcanzar sus objetivos académicos, profesionales y
							personales.
						</MissionVisionDescription>
					</MissionVisionColumn>
				</MissionVisionRow>
				<MissionVisionRow>
					<MissionVisionColumn>
						<MissionVisionDescription>
							La visión de ProQuest es ser un socio de confianza en la provisión
							de recursos de información de alta calidad, innovadores y
							accesibles, que enriquezcan la investigación, la educación y el
							conocimiento en todo el mundo.
						</MissionVisionDescription>
					</MissionVisionColumn>
					<img
						src='https://cefolog.weebly.com/uploads/5/8/4/8/58488755/vision-pagina_orig.png'
						alt='img'
					/>
				</MissionVisionRow>
			</MissionVisionContent>
		</GeneralSeccion>
	);
}

export default MisionAndVision;
