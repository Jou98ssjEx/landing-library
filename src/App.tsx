import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import LandingPage from "./pages/LandingPage";
import { BrowserRouter, Route, Router, Routes } from "react-router-dom";

function App() {
	// const [count, setCount] = useState(0)

	return (
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<LandingPage />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;
