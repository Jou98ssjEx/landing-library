import { Link } from "react-router-dom";
import {
	GeneralSeccion,
	LibrariesGrid,
	LibrariesTitle,
	LibraryCard,
	LibraryDescription,
	LibraryImage,
	LibraryTitle,
} from "./styled";

function DigitalLibrary() {
	const librariesData = [
		{
			id: 1,
			title: "JSTOR",
			description: "Description of Library 1",
			imageUrl:
				"https://www.ucl.ac.uk/library/sites/library/files/news-jstor-logo-horiz.gif",
			link: "https://www.jstor.org/",
		},
		{
			id: 2,
			title: "EBSCOhost",
			description: "Description of Library 2",
			imageUrl:
				"https://bae2008.files.wordpress.com/2019/04/featured-images-blog-bae-1.png",
			link: "https://www.ebsco.com/es/products/plataforma-de-investigacion-ebscohost",
		},
		{
			id: 3,
			title: "PubMed Central (PMC)",
			description: "Description of Library 1",
			imageUrl:
				"https://landportal.org/sites/landportal.org/files/styles/220heightmax/public/1200px-US-NLM-PubMed-Logo.svg_.png?itok=C79486wH",
			link: "https://www.ncbi.nlm.nih.gov/pmc/",
		},
		{
			id: 4,
			title: "ScienceDirect",
			description: "Description of Library 2",
			imageUrl: "https://nuaca.am/wp-content/uploads/2021/04/scopus.jpg",
			link: "https://www.sciencedirect.com/",
		},
		{
			id: 5,
			title: "IEEE Xplore",
			description: "Description of Library 1",
			imageUrl:
				"https://www.nihlibrary.nih.gov/sites/default/files/IEEE%20Web%20Ad.png",
			link: "https://ieeexplore.ieee.org/Xplore/home.jsp",
		},
		{
			id: 6,
			title: "SpringerLink",
			description: "Description of Library 2",
			imageUrl:
				"https://i0.wp.com/www.ucaldas.edu.co/portal/wp-content/uploads/2022/03/capacitacion-biblioteca.jpg?fit=700%2C400&ssl=1",
			link: "https://link.springer.com/",
		},
		{
			id: 7,
			title: "Taylor & Francis Online",
			description: "Description of Library 2",
			imageUrl:
				"https://www.ru.ac.za/media/rhodesuniversity/content/library/images/spotlight/T&F_Group_an_informa_business.JPG",
			link: "https://www.tandfonline.com/",
		},
		{
			id: 8,
			title: "Google Scholar",
			description: "Description of Library 2",
			imageUrl:
				"https://www.madrimasd.org/blogs/matematicas/files/2020/02/kisspng-google-search-google-analytics-marketing-business-google-scholar-logo-5b4c8647e1f404.7173265615317417679255.png",
			link: "https://scholar.google.com/",
		},
	];

	return (
		<GeneralSeccion id='libraries'>
			<LibrariesTitle>Bibliotecas Digitales</LibrariesTitle>
			<LibrariesGrid>
				{librariesData.map((library) => (
					<Link to={library.link} target='_blank'>
						<LibraryCard key={library.id}>
							<LibraryImage src={library.imageUrl} alt={library.title} />
							<LibraryTitle>{library.title}</LibraryTitle>
						</LibraryCard>
					</Link>
				))}
			</LibrariesGrid>
		</GeneralSeccion>
	);
}

export default DigitalLibrary;
