export const theme = {
  fonts: {
    primaryFont: "Raleway",
    secondaryFont: "Redig, Helvetica, sans serif",
    thirdFont: "Avenir, Helvetica, sans serif",
  },
  colors: {
    primaryColor: "",
    secondaryColor: "",
    animationBackground: "#aec6ef",
    secondaryBg: "#fbfbfb",
    white: "#fff",
    black: "#0c171d",
    blueColor: "#1593ff",
    grayText: "#7d7f80",
    grayText90: "#FFFFFFE5",
    footerColor: "#111a3a",
    blueLightColor: "#aec6ef",
    borderLightColor: "#ECECEF",
    tagBg: "#0c70bf",
    red: "#ff504f",
  },
};
