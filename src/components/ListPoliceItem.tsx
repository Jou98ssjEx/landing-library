import { useEffect, useState } from "react";
import {
	CircleItemContainer,
	ListItem,
	ListItemConent,
	PolicFirstItemContainer,
	PolicyDescription,
} from "./styled";

interface IListPoliceItem {
	item: {
		title: string;
		description: string;
	};
	idx: number;
}

function ListPoliceItem({ item, idx }: IListPoliceItem) {
	const [isSubItemsOpen, setIsSubItemsOpen] = useState(false);

	const toggleSubItems = () => {
		setIsSubItemsOpen(!isSubItemsOpen);
	};

	useEffect(() => {
		if (idx === 0) {
			setIsSubItemsOpen(true);
		}
	}, []);

	return (
		<>
			<PolicFirstItemContainer
				// style={{ justifyContent: "center" }}
				key={idx}
				onClick={toggleSubItems}>
				<CircleItemContainer> {idx + 1} </CircleItemContainer>
				<ListItem>
					<ListItemConent isActive={isSubItemsOpen}>
						<PolicyDescription>{item.title}</PolicyDescription>
						<i className='pi pi-chevron-right'></i>
					</ListItemConent>
				</ListItem>
			</PolicFirstItemContainer>
			{isSubItemsOpen && (
				<PolicFirstItemContainer
					style={{ marginRight: "5rem" }}
					key={idx}
					isActive>
					<PolicyDescription>{item.description}</PolicyDescription>
				</PolicFirstItemContainer>
			)}
		</>
	);
}

export default ListPoliceItem;
