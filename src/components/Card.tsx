import { CardContainer, CardDescription, CardTitle } from "./styled";

interface ICardProps {
	title: string;
	description: string;
}

function Card({ description, title }: ICardProps) {
	return (
		<CardContainer>
			<CardTitle>{title}</CardTitle>
			<CardDescription>{description}</CardDescription>
		</CardContainer>
	);
}

export default Card;
