import styled from "styled-components";

export const fadeIn = `
@keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  @-webkit-keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  -webkit-animation-duration: 0.5s;
  -webkit-animation-fill-mode: both;
  -webkit-animation-name: fadeIn;
  animation-duration: 0.5s;
  animation-fill-mode: both;
  animation-name: fadeIn;
`;

const linerTextColor = `
  background-clip: text;
  background-image: linear-gradient(90deg, #33f0fe 0%, #8280f9 100%) !important ;
  background: none;
  -moz-background-clip: text;
  -moz-text-fill-color: transparent;
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;

export const MainSection = styled.section`
	padding: 50px 0;
`;

export const MainTitle = styled.h2`
	font-size: 2.5rem;
	font-weight: bold;
	margin-bottom: 20px;
`;

export const SubTitle = styled.p`
	font-size: 1.2rem;
	color: #666;
`;

export const Container = styled.div`
	max-width: 1300px;
	margin: 0 auto;
	padding: 0 15px;
`;

// Header
export const HeaderContainer = styled.header<{ isBackgroundBlack?: boolean }>`
	/* background-color: rgba(0, 0, 0, 0.8); 
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	z-index: 1000; */
	background-color: ${(props) =>
		props.isBackgroundBlack ? props.theme.colors.black : "none"};
	display: flex;
	justify-content: center;
	margin: auto;
	position: ${(props) => (props.isBackgroundBlack ? "relative" : "absolute")};
	width: 100%;
	z-index: 1;
`;

export const HeaderContent = styled.nav`
	align-items: center;
	display: flex;
	justify-content: center;
	max-width: 130rem;
	padding: 3rem;
	width: 100%;
	@media (max-width: 768px) {
		width: 100%;
	}
`;

export const NavItems = styled.div`
	display: block;
	@media (max-width: 768px) {
		display: none;
	}
	ul {
		display: flex;
		gap: 4.2rem;
	}
`;

export const NavItem = styled.li<{ isBackgroundBlack?: boolean }>`
	list-style: none;

	span,
	a {
		color: ${(props) => props.theme.colors.white};
		cursor: pointer;
		font-family: ${(props) => props.theme.fonts.primaryFont};
		font-size: 1.6rem;
	}

	a:hover {
		color: ${(props) => props.theme.colors.blueColor};
	}

	a.active {
		color: ${(props) => props.theme.colors.blueColor};
	}
	ul {
		backdrop-filter: blur(8.5px);
		display: none;
		list-style: none;
		min-width: 14rem;
		padding: 2rem 1rem;
		position: absolute;
		${(props) =>
			props.isBackgroundBlack &&
			`
      background-color: ${props.theme.colors.black};
      box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
      `}
	}
	&:hover > ul {
		display: block;
	}
	ul li {
		margin-bottom: 1rem;
		position: relative;
		&:last-child {
			margin-bottom: 0rem;
		}
	}
`;

export const NavItemMobile = styled.li`
	list-style: none;
	transition: all 0.3s ease-in-out;
	span,
	a {
		color: ${(props) => props.theme.colors.black};
		font-family: ${(props) => props.theme.fonts.primaryFont};
		font-size: 1.6rem;
	}

	a:hover {
		color: ${(props) => props.theme.colors.blueColor};
		cursor: pointer;
	}

	a.active {
		color: ${(props) => props.theme.colors.blueColor};
	}
	ul {
		display: none;
		list-style: none;
		margin-left: 1.5rem;
		min-width: 14rem;
		padding-top: 2rem !important;
	}
	ul li {
		border-bottom: none !important;
		padding-bottom: 2rem !important;
		padding: 0rem !important;
		&:last-child {
			padding-bottom: 0rem !important;
		}
	}
`;
export const MenuBurger = styled.div`
	display: none;
	@media (max-width: 768px) {
		display: block;
	}
`;

export const HeaderMenuMobile = styled.div<{ visibleMenu: boolean }>`
	@media (max-width: 768px) {
		display: ${(props) => (props.visibleMenu ? "flex" : "none")};
		align-items: center;
		background-color: ${(props) => props.theme.colors.white};
		bottom: 0;
		height: 100%;
		justify-content: center;
		margin-bottom: 3rem;
		position: fixed;
		top: 0;
		width: 100%;
		z-index: 100;
	}
`;

export const MenuContainer = styled.div`
	display: flex;
	padding: 4rem;
	width: 100%;
	ul {
		display: flex;
		flex-direction: column;
		width: 100%;
		li {
			border-bottom: 1px solid ${(props) => props.theme.colors.borderLightColor};
			padding: 2rem 0rem;
			width: 100%;
		}
	}
	@media (min-width: 768px) {
		display: none;
	}
`;

// export const Nav = styled.nav`
// 	display: flex;
// 	justify-content: space-around;
// 	align-items: center;
// 	height: 80px; /* Ajusta la altura del header según tus necesidades */
// 	font-family: "Arial", sans-serif; /* Cambia la fuente según tu preferencia */
// `;

// export const NavLink = styled.a`
// 	color: #fff;
// 	text-decoration: none;
// 	font-size: 1.2rem;
// 	transition: all 0.3s ease-in-out;

// 	&:hover {
// 		color: #ffd700; /* Cambia el color al pasar el ratón */
// 	}
// `;

// Estilo para el logo si lo tienes
export const Logo = styled.img`
	width: 50px;
	height: 50px;
`;

// Ejemplo de estilo para un elemento del navbar activo
// export const ActiveLink = styled(NavLink)`
// 	color: #ffd700; /* Cambia el color del elemento activo */
// `;

// cards
// export const CardContainer = styled.div`
// 	background-color: #fff;
// 	border-radius: 8px;
// 	box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
// 	padding: 20px;
// 	transition: all 0.3s ease;

// 	&:hover {
// 		transform: translateY(-5px);
// 		box-shadow: 0 6px 12px rgba(0, 0, 0, 0.2);
// 	}
// `;

// export const CardTitle = styled.h3`
// 	font-size: 1.5rem;
// 	margin-bottom: 10px;
// `;

// export const CardDescription = styled.p`
// 	font-size: 1rem;
// `;

export const SectionContainer = styled.section`
	background-color: #f7f7f7;
	padding: 50px 0;
`;

export const SectionContent = styled.div`
	max-width: 1300px;
	margin: 0 auto;
	padding: 0 20px;
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
`;

export const SectionTitle = styled.h2`
	font-size: 3rem;
	font-weight: bold;
	margin-bottom: 40px;
	text-align: center;
	width: 100%;
`;

export const CardContainer = styled.div`
	background-color: #fff;
	border-radius: 8px;
	box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
	padding: 20px;
	margin: 10px;
	display: flex;
	gap: 2rem;
	flex-direction: column;

	width: 300px; /* Ajusta el ancho según tus necesidades */
	transition: all 0.3s ease;

	img {
		border-radius: 1rem;
		width: 100%;
	}

	@media (max-width: 768px) {
		width: 100%; /* Cambia el ancho en pantallas pequeñas */
	}

	&:hover {
		transform: translateY(-5px);
		box-shadow: 0 6px 12px rgba(0, 0, 0, 0.2);
	}
`;

export const CardTitle = styled.h3`
	${linerTextColor};
	text-align: center;
	font-size: 2.4rem;
	margin-bottom: 10px;
`;

export const CardDescription = styled.p`
	font-size: 1.4rem;
	text-align: justify;
`;

//

export const GeneralSeccion = styled.section<{ bg?: string }>`
	/* background: ${(props) => props.theme.colors.secondaryBg}; */
	background: ${(props) => props.bg || "#e1e1e1"};
	padding: 5.6rem 0rem;
	width: 100%;
	@media (max-width: 768px) {
		padding: 5rem 0rem;
	}
`;

export const MissionVisionContent = styled.div`
	max-width: 1300px;
	margin: 0 auto;
	padding: 0 20px;
`;

export const MissionVisionRow = styled.div`
	display: grid;
	gap: 2.5rem;
	place-items: center;
	grid-template-columns: repeat(2, 1fr);
	@media (max-width: 568px) {
		grid-template-columns: repeat(1, 1fr);
	}
`;

export const MissionVisionColumn = styled.div`
	display: flex;
	align-items: center;
	background: #f1f1f17c;
	padding: 2rem;
	border-radius: 1.6rem;
	height: 60%;
	box-shadow: rgb(204, 219, 232) 3px 3px 6px 0px inset,
		rgba(255, 255, 255, 0.5) -3px -3px 6px 1px inset;
`;

export const MissionVisionTitle = styled.h2`
	color: #000;
	font-size: 3rem;
	font-weight: bold;
	margin-bottom: 20px;
	text-align: center;
`;

export const MissionVisionDescription = styled.p`
	align-items: center;
	text-align: justify;
	font-size: 2rem;
`;

//
export const PoliciesContent = styled.div`
	max-width: 1300px;
	margin: 0 auto;
	padding: 0 20px;
`;

export const PolicyRow = styled.div`
	display: flex;

	flex-direction: column;
	width: 100%;
	gap: 2.4rem;
`;

export const PolicyColumn = styled.div`
	margin-bottom: 20px;

	@media (max-width: 768px) {
		width: 100%;
	}
`;

export const PolicyTitle = styled.h2`
	font-size: 3rem;
	text-align: center;
	font-weight: bold;
	margin-bottom: 40px;
`;

export const PolicyItem = styled.div`
	background-color: #fff;
	border-radius: 8px;
	padding: 20px;
	box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
	position: relative;
	cursor: pointer;
	width: 100%;
`;

export const PolicyNumber = styled.div`
	font-size: 1.5rem;
	font-weight: bold;
	margin-bottom: 10px;
`;

export const PolicyDescription = styled.p`
	font-size: 2rem;
`;

export const SubItemsContainer = styled.ul<{ isOpen: boolean }>`
	position: absolute;
	top: 100%;
	left: 0;
	background-color: #fff;
	border-radius: 8px;
	box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
	padding: 10px 20px;
	list-style: none;
	display: ${({ isOpen }: { isOpen: boolean }) => (isOpen ? "block" : "none")};
`;

export const SubItem = styled.li`
	font-size: 0.9rem;
	margin-bottom: 5px;
`;

//

export const LibrariesContainer = styled.section`
	background-color: #f9f9f9;
	padding: 50px 0;
	text-align: center;
`;

export const LibrariesTitle = styled.h2`
	font-size: 3rem;
	font-weight: bold;
	margin: 50px auto;
	text-align: center;
	max-width: 1300px;
`;

export const LibrariesGrid = styled.div`
	display: grid;
	grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
	gap: 20px;
	max-width: 1300px;
	margin: 0 auto;
	padding: 0 20px;
`;

export const LibraryCard = styled.div`
	background-color: #fff;

	border-radius: 8px;
	box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
	padding: 20px;
	transition: all 0.3s ease;
	cursor: pointer;

	&:hover {
		transform: translateY(-5px);
		box-shadow: 0 6px 12px rgba(0, 0, 0, 0.2);
	}
`;

export const LibraryImage = styled.img`
	border-radius: 8px;
	height: 150px;
	margin-bottom: 10px;
	width: 100%;
`;

export const LibraryTitle = styled.h3`
	${linerTextColor}
	font-size: 1.5rem;
	text-align: center;
	margin-bottom: 10px;
`;

export const LibraryDescription = styled.p`
	font-size: 1rem;
	margin-bottom: 15px;
`;

// footer
export const FooterContainer = styled.footer`
	background-color: #333;
	color: #fff;
	padding: 50px 0;
`;

export const FooterContent = styled.div`
	max-width: 1300px;
	margin: 0 auto;
	padding: 0 20px;
	display: flex;
	flex-direction: column;
	align-items: center;
`;

export const FooterTitle = styled.h2`
	font-size: 3rem;
	font-weight: bold;
	margin-bottom: 40px;
`;

export const FooterRow = styled.div`
	display: grid;
	grid-template-columns: repeat(3, 1fr);
	gap: auto;
	width: 100%;
`;

export const FooterColumn = styled.div`
	/* width: 48%; */
	display: flex;
	gap: rem;
	align-items: center;
	flex-direction: column;

	@media (max-width: 768px) {
		width: 100%;
		text-align: center;
		margin-bottom: 20px;
	}
`;

export const StudentImage = styled.img`
	width: 150px;
	height: 150px;
	border-radius: 1.6rem;
	margin-bottom: 10px;
`;

export const FooterText = styled.p`
	font-size: 1.5rem;
	margin-bottom: 10px;
	color: white;
	font-weight: 700;
`;
export const FooterText2 = styled.p`
	color: #f2f2f2;
	font-size: 1.2rem;
	margin-bottom: 10px;
`;

export const Copyright = styled.p`
	color: white;
	font-size: 1.5rem;
	margin-top: 4rem;
`;

// button float
export const ScrollButton = styled.button<{ visible: boolean }>`
	position: fixed;
	bottom: 20px;
	right: 20px;
	background-color: #333;
	color: #fff;
	border: none;
	border-radius: 50%;
	width: 50px;
	height: 50px;
	cursor: pointer;
	display: ${(props: { visible: boolean }) =>
		props.visible ? "block" : "none"};
	opacity: ${(props: { visible: boolean }) => (props.visible ? "1" : "0")};
	transition: opacity 0.3s ease;

	&:hover {
		background-color: #555;
	}
`;

export const PolicFirstItemContainer = styled.div<{
	isActive?: boolean;
}>`
	${fadeIn};
	align-items: center;
	display: flex;
	gap: 1.6rem;

	${(props) =>
		props.isActive &&
		`
		background-color: #fff;
		border-radius: 8px;
		padding: 20px;
		box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);

	`}
`;

export const CircleItemContainer = styled.div`
	align-items: center;
	background-color: #212132;
	border-radius: 50%;
	color: white;
	display: flex;
	font-size: 2rem;
	height: 5rem;
	justify-content: center;
	text-align: center;
	width: 5rem;
`;

export const ListItem = styled.div<{
	isActive?: boolean;
}>`
	display: flex;
	/* height: 5rem; */
	/* width: 100%; */

	${(props) =>
		props.isActive &&
		`
		
		background-color: #fff;
		border-radius: 8px;
		padding: 20px;
		box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
		
		`}
	position: relative;
	flex-direction: column;
	cursor: pointer;
`;

export const ListItemConent = styled.div<{ isActive?: boolean }>`
	display: flex;
	gap: 1rem;
	align-items: center;

	i {
		transform: ${(props) => props.isActive && "rotate(90deg)"};
		font-size: 1.3rem;
		font-weight: 700;
	}
`;
