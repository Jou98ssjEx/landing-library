import {
	CardContainer,
	CardDescription,
	CardTitle,
	GeneralSeccion,
	SectionContainer,
	SectionContent,
	SectionTitle,
} from "./styled";

function UserTypes() {
	const data = [
		{
			title: "Estudiantes",
			img: "https://tecnosoluciones.com/wp-content/uploads/2022/12/participacion-clases-dinamicas-sesion-virtual.png",
			description:
				"Personas que buscan información para proyectos escolares, tareas académicas y estudios.",
		},
		{
			title: "Investigadores",
			img: "https://contactomaestro.colombiaaprende.edu.co/sites/default/files/maestrospublic/styles/interna_850x260/public/2021-01/Imagen-Convocatoria-Maestro-Investigador.png?h=f52a42fa&itok=JzWA5Wsw",
			description:
				" Profesionales y académicos que realizan investigaciones en diversas áreas del conocimiento.",
		},
		{
			title: "Profesores y educadores",
			img: "https://www.magisnet.com/wp-content/uploads/2021/04/Streamers.jpg",
			description:
				"Personas que utilizan recursos académicos para enseñanza, planificación de cursos y desarrollo curricular.",
		},
		{
			title: "Profesionales",
			img: "https://www.aicad.es/asset/img/3/practicas-profecionales.png",
			description:
				"Individuos que buscan información relevante para sus campos de trabajo y desarrollo profesional.",
		},
	];

	return (
		<GeneralSeccion id='users'>
			<SectionContent>
				<SectionTitle>Tipos de Usuarios</SectionTitle>
				{data.map((item, idx) => (
					<CardContainer key={idx}>
						<img src={item.img} alt='' />
						<div>
							<CardTitle>{item.title}</CardTitle>
							<CardDescription>{item.description}</CardDescription>
						</div>
					</CardContainer>
				))}
			</SectionContent>
		</GeneralSeccion>
	);
}

export default UserTypes;
